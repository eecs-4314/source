import sys
import csv

understand = csv.reader(sys.stdin)
next(understand)
for line in understand:
    from_file = line[0]
    intellij_slug = "RELATIVE:/intellij-community-idea-192.6603.8/"
    to_file = line[1].split()
    to_file = to_file[-1]
    if '/src/' in to_file:
        index = to_file.index('/src/')
        to_file = to_file[index+5:]
        entry = from_file.replace(intellij_slug, "") + " " + to_file
        print(entry)