#!/usr/bin/python
"""importmapper.py"""

import sys

#function for extract file name
def cleanFile(filename):
	s = ''
	index =  filename.find('"', 2)
	result = filename[1:index]
    # result = result.replace("intellij-community-idea-192.6603.8/", "")
	return result

#function for get a entir path form xml format
def getImportPath(impor):
        intel = '/intellij'
	s = ''
        for i in impor:
            if i!='':
                index =  i.find('</')
                s=s+'/'+i[:index]
        if intel in s:
            if s[-1]!='*':
                s=s+'.java'
                return s[1:]
            else:
                s=''
                return s
        else:
            return ''

#function for tokenize string
def get(term):
    splited=[]
    for t in term:
        sou = t.split('<name>')[0]
        sou = cleanFile(sou)
        imports = t.split('<name>')[1:]
        imports = getImportPath(imports)
        if imports!='':
            splited.append(sou.replace("intellij-community-idea-192.6603.8/", "")+' '+str(imports))
    return splited


#mapper function for this importmapper
for line in sys.stdin:
	line = line.strip()
	term = line.split('filename=',1)[1:2]
	words = get(term)
	for word in words:
		print ('%s\t%s' % (word,1))
