srcMl
    1. Generate XML file from source code
        srcml --verbose "SOURCE CODE" -o intellij.xml
    2. Get all import from XML file
        srcml --xpath "//src:import/src:name" intellij.xml

IMPORTS:
    1. Get all dependencies
        run "./importmapper.py < importname.txt | sort | ./importreducer.py" to get "import.txt"
    2. Get full path
        run "./importPath.py < import.txt > importoutput.txt" to get "importoutput.txt" 