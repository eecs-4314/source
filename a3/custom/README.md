## Custom Dependency Extraction Method


usage: deps.py [-h] path

prints dependency tuple-attribute pairs to stdin

positional arguments:
  path        canonical path to intellij source code root

### Generating output
`python3 deps.py <INTELLIJ-ROOT-DIR-PATH> > outputs/custom.ta`