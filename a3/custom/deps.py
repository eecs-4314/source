import os
import re
import argparse

parser = argparse.ArgumentParser(description='prints dependency tuples to stdin')
parser.add_argument("path", help="canonical path to intellij source code root", type=str)
args = parser.parse_args()
if not args.path:
    print("IllegalArgumentException")
    exit
        
rootdir = str(args.path)

pattern = re.compile("^import.*(intellij)")
for subdir, dirs, files in os.walk(rootdir):
    for file in files:

        file_path = os.path.join(subdir, file)

        if file_path.split(".")[-1] == "java":
            try:
                with open(file_path, "r+") as fo:
                    for index in range(100):
                        line = fo.readline()
                        if pattern.match(line):
                            importOrpackage = line.split(" ")[1].replace(";", "").replace(".", "/").replace("\n", "")
                            temp = file_path.replace(rootdir, "")
                            if "*" not in importOrpackage:
                                print(temp.strip() + " " + importOrpackage.strip() + ".java")
            except:
                pass